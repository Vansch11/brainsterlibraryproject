$(function () {
  fetch("server.php")
    .then(function (data) {
      return data.json();
    })
    .then(function (data) {
      console.log(data);

      const books = data;

      books.forEach((book) => {
        $("#bookList").append(
          `
          <div id="${book["id"]}" class="card-group col-sm-12 col-md-6 col-lg-3 mb-5 ${book["bookCategory"]}">
                <div class="card">
                    <a href="#" class="d-block text-dark text-decoration-none">
                        <div>
                            <img class="card-img-top" src="${book["picture"]}" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="text-dark font-weight-bold px-2">
                            ${book["title"]}
                            </h5>
                            <p class="text-muted">Author: <i> ${book["authorName"]} ${book["authorSurname"]}</i></p>
                            <div class="stars">
                              <i class="fa fa-star"> </i>
                              <i class="fa fa-star"> </i>
                              <i class="fa fa-star"> </i>
                              <i class="fa fa-star"> </i>
                              <i class="fa fa-star"> </i>
                            </div>
                            <p class="mb-0">${book["bookCategory"]}</p>
                            <span><small> Published: ${book["year"]}</span>
                            
                        </div>
                    </a>
                </div>
            </div> `
        );
      });
    })
    .catch(function (err) {
      console.log(err);
    });

  //  Filters for checkboxes

  $('input[type="checkbox"]').each(function () {
    $(this).change(function () {
      let id = this.id;
      let card = $("." + id);
      if ($(this).is(":checked")) {
        console.log("Checkbox " + this.id + " checked");
        card.css("display", "flex");
      } else {
        console.log("Checkbox " + this.id + " unchecked");
        card.css("display", "none");
      }
    });
  });

  //  event on each book

  $("body").on("click", ".card-group", (e) => {
    $id = e.currentTarget.id;
    window.open(`book.php?id=${e.currentTarget.id}`);
  });

  $("body").on("mouseover", ".card-group", function () {
    $(this).css("transform", "scale(1.1)");
    $(this).css("transition", "500ms ease-in-out 25ms");
  });

  $("body").on("mouseleave", ".card-group", function () {
    $(this).css("transform", "scale(1)");
  });



});
