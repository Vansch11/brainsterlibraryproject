<?php

require_once __DIR__ . "/../functions.php";

session_init();

session_destroy();

header("Location: ./../index.php");
