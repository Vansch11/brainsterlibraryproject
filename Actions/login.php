<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../conn.php';
    require_once __DIR__ . "/../functions.php";

    session_init();


    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM users WHERE email = :email LIMIT 1";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['email' => $email]);

    if ($stmt->rowCount() == 1) {
        $user = $stmt->fetch();

        if (password_verify($password, $user['password'])) {
            $_SESSION['username'] = $user['username'];
            $_SESSION['userid'] = $user['id'];
            $_SESSION['role'] = $user['role_id'];

            header('Location: ./../index.php');
            die();
        } else {
            $_SESSION['error'] = 'Wrong email and password combination!';

            header('Location: ./../login.php');
            die();
        }
    } else {
        $_SESSION['error'] = 'Wrong credentials!';

        header("Location: ./../login.php");
        die();
    }
} else {
    header('Location: ./../index.php');
    die();
}
