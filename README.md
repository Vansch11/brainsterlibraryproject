# Brainster Library

This project is an online library that offers a platform where the end users can create a profile which allows them to leave public comments for the books they have read. Also users have an option to take private notes for each book they are reading at the moment, revisit them at any time, edit/delete options for each note as well. 

Admins on the platform can manage all the books, book categories, authors and the public comments from the users. They have access to the menu with the CRUD for each source.

## Installation

1. Clone the repository

-git clone https://gitlab.com/aleksandar.nikolovski/brainsterprojects_aleksandarnikolovski_fs9.git

## Usage

After installing uste the command: -git checkout Project2

Create the database by inserting the queries from the db.sql file.

In users table pre-populate one admin with role_id '1', username, email and hashed password.


## Build with

-PHP
-JQuery


## Contact

For more information or support, please contact [Aleksandar](aleksandar.nik94@gmail.com).
