<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_POST['bookId'])) {
    require_once __DIR__ . "/../conn.php";

    $bookId = $_POST['bookId'];

    $sql = "UPDATE books SET
            is_deleted = :is_deleted 
            WHERE id = :id";
    $stmt = $pdo->prepare($sql);

    if ($stmt->execute(['id' => $bookId, 'is_deleted' => 1])) {

        echo json_encode(["status" => "success"]);
    } else {
        echo json_encode(["status" => "error"]);
    };
}
