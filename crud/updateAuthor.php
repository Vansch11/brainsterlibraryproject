<?php

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    require_once __DIR__ . "/../conn.php";

    $sql = "UPDATE authors SET
    name = :name,
    surname = :surname,
    biography = :biography,
    is_deleted = :is_deleted
    WHERE id = :id";

    $stmt = $pdo->prepare($sql);
    if ($stmt->execute([
        'id' => $_POST['id'], 'name' => $_POST['name'], 'surname' => $_POST['surname'],
        'biography' => $_POST['biography'], 'is_deleted' => $_POST['is_deleted']
    ])) {

        header("Location: ./createAuthor.php");
    };
} else {
    header("Location: ./../index.php");
    die();
}
