<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $sql = "SELECT * FROM authors";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }
} else {
    header("Location: ./../index.php");
    die();
}



?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4 offset-3">
                        <h1>Create Author</h1>
                        <form action="./storeAuthor.php" method="POST">
                            <input type="hidden" name="is_deleted" value="0">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control <?php if (isset($_SESSION['name'])) echo 'is-invalid' ?>" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter name">

                                <?php if (isset($_SESSION['name'])) { ?>
                                    <div class="invalid-feedback">
                                        <?= $_SESSION['name'] ?>
                                    </div>
                                <?php }
                                unset($_SESSION['name']); ?>
                            </div>
                            <div class="form-group">
                                <label for="surname">Surname</label>
                                <input type="text" class="form-control <?php if (isset($_SESSION['surname'])) echo 'is-invalid' ?>" id="surname" name="surname" aria-describedby="emailHelp" placeholder="Enter surname">

                                <?php if (isset($_SESSION['surname'])) { ?>
                                    <div class="invalid-feedback">
                                        <?= $_SESSION['surname'] ?>
                                    </div>
                                <?php }
                                unset($_SESSION['surname']); ?>
                            </div>
                            <div class="form-group">
                                <label for="biography">Biography</label>
                                <textarea class="form-control <?php if (isset($_SESSION['biography'])) echo 'is-invalid' ?>" id="biography" name="biography" rows="3" placeholder="Type biography"></textarea>

                                <?php if (isset($_SESSION['biography'])) { ?>
                                    <div class="invalid-feedback">
                                        <?= $_SESSION['biography'] ?>
                                    </div>
                                <?php }
                                unset($_SESSION['biography']); ?>
                            </div>

                            <button type="submit" class="btn btn-success">Store Author</button>
                        </form>
                        <div>
                            <a class="btn btn-danger mt-3" href="./../index.php">Back</a>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="row">
                            <div class="col-10">
                                <h1>Authors</h1>
                            </div>

                            <div class="col-6">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">Authors</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while ($author = $stmt->fetch()) { ?>
                                            <tr>
                                                <th><?= $author['id'] ?></th>
                                                <td> <?php echo  $author['name'];
                                                        echo $author['surname'] ?></td>
                                                <td> <?= $author['biography'] ?></td>
                                                <td>
                                                    <a href="editAuthor.php?id=<?= $author['id'] ?>" class="btn btn-warning mt-2">Edit</a>
                                                    <?php if ($author['is_deleted'] == 0) { ?>
                                                        <a href="deleteAuthor.php?id=<?= $author['id'] ?>" class="btn btn-danger mt-2">Delete</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>