<?php
require_once __DIR__ . "/../functions.php";
session_init();


if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if ($_POST['name'] == "") {
        $_SESSION['name'] = 'Name field cannot be empty';
        header("Location: createAuthor.php");
        die();
    }
    if ($_POST['surname'] == "") {
        $_SESSION['surname'] = 'Surname field cannot be empty';
        header("Location: createAuthor.php");
        die();
    }

    if (empty($_POST['biography'])) {
        $_SESSION['biography'] = 'Biography field cannot be empty';
        header("Location: createAuthor.php");
        die();
    }



    require_once __DIR__ . "/../conn.php";

    $sql = "INSERT INTO authors (name, surname, biography, is_deleted) VALUES (:name, :surname, :biography, :is_deleted)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        'name' => $_POST['name'], 'surname' => $_POST['surname'],
        'biography' => $_POST['biography'], 'is_deleted' => $_POST['is_deleted']
    ]);

    header("Location: ./createAuthor.php");
} else {
    header("Location: ./../index.php");
    die();
}
