<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $sql = "SELECT * FROM authors WHERE id = :id LIMIT 1";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id' => $_GET['id']]);

        $author = $stmt->fetch();
    }
} else {
    header("Location: ./../index.php");
    die();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

    <style>
        body {
            background-color: grey;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4 offset-3">
                        <h1>Edit Author</h1>
                        <form action="updateAuthor.php" method="POST">
                            <input type="hidden" name="id" value="<?= $author['id'] ?>">
                            <input type="hidden" name="is_deleted" value="0">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="<?= $author['name'] ?>" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="surname">Surname</label>
                                <input type="text" class="form-control" id="surname" name="surname" value="<?= $author['surname'] ?>" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="biography">Biography</label>
                                <textarea class="form-control" id="biography" name="biography" rows="3"> <?= $author['biography'] ?></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Update Author</button>
                        </form>
                        <div>
                            <a class="btn btn-danger mt-3" href="./createAuthor.php">Back</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>