<?php
require_once __DIR__ . "/../functions.php";
session_init();

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if ($_POST['title'] == "") {
        $_SESSION['title'] = 'Title field cannot be empty';
        header("Location: createBook.php");
        die();
    }
    if ($_POST['author'] == "") {
        $_SESSION['author'] = 'Author must be selected';
        header("Location: createBook.php");
        die();
    }
    if ($_POST['year'] == "") {
        $_SESSION['year'] = 'Year field cannot be empty';
        header("Location: createBook.php");
        die();
    }
    if ($_POST['pages'] == "") {
        $_SESSION['pages'] = 'Pages field cannot be empty';
        header("Location: createBook.php");
        die();
    }
    if ($_POST['picture'] == "") {
        $_SESSION['picture'] = 'Picture field cannot be empty';
        header("Location: createBook.php");
        die();
    }
    if ($_POST['category'] == "") {
        $_SESSION['category'] = 'Category must be selected';
        header("Location: createBook.php");
        die();
    }

    require_once __DIR__ . "/../conn.php";

    $sql = "INSERT INTO books (title, author_id, year, pages, picture, category_id, is_deleted) VALUES
         (:title, :author_id, :year, :pages, :picture, :category_id, :is_deleted)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        'title' => $_POST['title'], 'author_id' => $_POST['author'], 'year' => $_POST['year'],
        'pages' => $_POST['pages'], 'picture' => $_POST['picture'], 'category_id' => $_POST['category'], 'is_deleted' => 0
    ]);

    header("Location: createBook.php");
    die();
} else {
    header("Location: ./../index.php");
    die();
}
