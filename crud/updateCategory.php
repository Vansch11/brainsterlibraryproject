<?php

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    require_once __DIR__ . "/../conn.php";

    $sql = "UPDATE categories SET
    category = :category,
    is_deleted = :is_deleted
    WHERE id = :id";

    $stmt = $pdo->prepare($sql);
    if ($stmt->execute(['id' => $_POST['id'], 'category' => $_POST['category'], 'is_deleted' => $_POST['is_deleted']])) {

        header("Location: ./createCategory.php");
    };
} else {
    header("Location: ./../index.php");
    die();
}
