<?php

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    require_once __DIR__ . "/../conn.php";

    $sql = "UPDATE books SET
    title = :title,
    author_id = :author_id,
    year = :year,
    pages = :pages,
    picture = :picture,
    category_id = :category_id,
    is_deleted = :is_deleted
    WHERE id = :id";

    $stmt = $pdo->prepare($sql);
    if ($stmt->execute([
        'id' => $_POST['id'], 'title' => $_POST['title'], 'author_id' => $_POST['author'], 'year' => $_POST['year'],
        'pages' => $_POST['pages'], 'picture' => $_POST['picture'], 'category_id' => $_POST['category'], 'is_deleted' => 0
    ])) {

        header("Location: ./createBook.php");
    };
} else {
    header("Location: ./../index.php");
    die();
}
