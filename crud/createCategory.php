<?php

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

        $sql = "SELECT * FROM categories";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }
} else {
    header("Location: ./../index.php");
    die();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4 offset-3">
                        <h1>Create Category</h1>
                        <form action="storeCategory.php" method="POST">
                            <input type="hidden" name="is_deleted" value="0">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" id="category" name="category" aria-describedby="emailHelp" placeholder="Type in">
                            </div>
                            <button type="submit" class="btn btn-success">Store Category</button>
                        </form>
                        <div>
                            <a class="btn btn-danger mt-3" href="./../index.php">Back</a>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="row">
                            <div class="col-7">
                                <h1>Categories</h1>
                            </div>

                            <div class="col-6">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while ($category = $stmt->fetch()) { ?>
                                            <tr>
                                                <th><?= $category['id'] ?></th>
                                                <td> <?= $category['category'] ?></td>
                                                <td>
                                                    <a href="editCategory.php?id=<?= $category['id'] ?>" class="btn btn-warning">Edit</a>
                                                    <?php if ($category['is_deleted'] == 0) { ?>
                                                        <a href="deleteCategory.php?id=<?= $category['id'] ?>" class="btn btn-danger">Delete</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>