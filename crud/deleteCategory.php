<?php 

require_once __DIR__ . "/../functions.php";
session_init();

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 1) {

        require_once __DIR__ . "/../conn.php";

         $sql = "UPDATE categories SET
         is_deleted = :is_deleted 
         WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([ 'id' => $_GET['id'], 'is_deleted' => 1]);

        header("Location: ./createCategory.php");
        die();
    }
} else {
    header("Location: ./../index.php");
    die();
}
