<?php
require_once __DIR__ . "/functions.php";

require_once 'conn.php';
session_init();


$categories = "SELECT * FROM categories
WHERE is_deleted = 0";
$stmtCategories = $pdo->prepare($categories);
$stmtCategories->execute();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Project2</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous" />
    <script src="https://kit.fontawesome.com/c22ff2489d.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="index.css">

</head>

<body>

    <!-- Nav bar -->

    <?php
    include_once __DIR__ . "/layouts/navbar.php";
    ?>
    <!-- Nav bar end  -->


    <!-- checkboxes -->
    <div class="container-fluid sticky-top bg-darkL text-white mt-2 mb-4">
        <div class="row p-3">

            <?php while ($rows = $stmtCategories->fetch()) {  ?>
                <div class="form-check ml-3">

                    <input class="form-check-input " type="checkbox" value="" id="<?= $rows['category'] ?>" checked>
                    <label class="form-check-label " for="<?= $rows['category'] ?>">
                        <?= $rows['category'] ?>
                    </label>
                </div>
            <?php } ?>

        </div>
    </div>
    <!-- end checkboxes -->


    <!-- Book listed  -->
    <div class="container mt-3">
        <div class="row justify-content-start " id="bookList">

        </div>
    </div>
    <!-- end Book listed -->


    <!-- footer -->
    <?php
    require_once __DIR__ . "/layouts/footer.php";
    ?>
    <!-- end footer  -->


    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="index.js"></script>
    <script src="layouts/footerQuote.js"></script>


</body>

</html>