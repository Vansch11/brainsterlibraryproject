<?php

require_once 'const.php';

try {
    $pdo = new PDO("mysql:host=" . HOST . ';dbname=' . DB_NAME, USERNAME, PASSWORD, [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
} catch (PDOException $e) {
    echo 'Connection faild!';
    die();
}