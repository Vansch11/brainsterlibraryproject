<?php

require_once __DIR__ . "./conn.php";

// get comments 
if (isset($_POST['action']) && $_POST['action'] == "getComments") {

    $userid = $_POST['userid'];
    $bookid = $_POST['bookid'];

    $sql = "SELECT * FROM comments WHERE user_id = $userid  AND book_id = $bookid AND approved = 1 ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $comments = $stmt->fetch();

    echo json_encode($comments);
}


//storing comment
if (isset($_POST['comment'])) {


    $comment = $_POST['comment'];
    $userid = $_POST['userid'];
    $bookid = $_POST['bookid'];


    $sql = "INSERT INTO comments (user_id, book_id, comment, approved) VALUES (:user_id, :book_id, :comment, :approved)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        'user_id' => $userid, 'book_id' => $bookid,
        'comment' => $comment, 'approved' => 1
    ]);

    $lastId = $pdo->lastInsertId();

    echo json_encode($lastId);
}

// remove comment
if (isset($_POST['action']) && $_POST['action'] == "removeComment") {


    $commentId = $_POST['commentId'];

    $sql = "DELETE FROM comments WHERE id = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $commentId]);
}
