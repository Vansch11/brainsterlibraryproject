<?php

require_once 'conn.php';

if (isset($_POST['notes'])) {

  $userid = $_POST['userid'];
  $bookid = $_POST['bookid'];
  $note = $_POST['notes'];

  $sql = "INSERT INTO notes (user_id, book_id, note) VALUES (:user_id, :book_id, :note)";
  $stmt = $pdo->prepare($sql);
  $stmt->execute(['user_id' => $userid, 'book_id' => $bookid, 'note' => $note]);

  $lastId = $pdo->lastInsertId();

  echo json_encode($lastId);
}


if (isset($_POST['action']) && $_POST['action'] == "getNotes") {

  $userid = $_POST['userid'];
  $bookid = $_POST['bookid'];

  $sql = "SELECT * FROM notes WHERE user_id = $userid  AND book_id = $bookid ";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $notes = $stmt->fetchAll();

  echo json_encode($notes);
}

//delete note

if (isset($_POST["delete"]) && $_POST["delete"] == "delete") {

  $noteId = $_POST["noteId"];

  $sql = "DELETE FROM notes WHERE id = $noteId";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
}

//update note


if (isset($_POST['updatedNote'])) {

  $noteId = $_POST["id"];
  $updatedText = $_POST["updatedNote"];

  $sql = "UPDATE notes SET
  note = :note
  WHERE id = :id";

  $stmt = $pdo->prepare($sql);
  $stmt->execute(['id' => $noteId, 'note' => $updatedText]);
}
