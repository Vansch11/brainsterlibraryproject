CREATE TABLE roles (
    id INT unsigned PRIMARY KEY AUTO_INCREMENT,
    role VARCHAR(32)
    )

    INSERT INTO `roles`(`role`) VALUES ('admin')
    INSERT INTO `roles`(`role`) VALUES ('regular')


CREATE TABLE users (
    id INT unsigned PRIMARY KEY AUTO_INCREMENT,
    role_id INT UNSIGNED,
    username VARCHAR(32),
    email VARCHAR (32),
    password VARCHAR(64),
    
    CONSTRAINT FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE
    )


CREATE TABLE authors (
    id INT unsigned  PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(32),
    surname VARCHAR(32),
    biography VARCHAR(255),
    is_deleted INT
   )
    
CREATE TABLE categories (
    id INT unsigned PRIMARY KEY AUTO_INCREMENT,
    category VARCHAR(32),
    is_deleted INT
    )


    CREATE TABLE books (
	id INT unsigned PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(64),
    author_id INT UNSIGNED,
    year INT,
    pages INT,
    picture VARCHAR(255),
    category_id INT UNSIGNED,
    is_deleted INT,
    
	CONSTRAINT FOREIGN KEY (author_id) REFERENCES authors(id) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE
)


CREATE TABLE comments (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id INT UNSIGNED,
    book_id INT UNSIGNED,
    comment VARCHAR(255),
    approved TINYINT,
    
    
    CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (book_id) REFERENCES books(id) ON DELETE CASCADE
)

CREATE table notes (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id INT UNSIGNED,
    book_id INT UNSIGNED,
    note VARCHAR(255),
   
    CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (book_id) REFERENCES books(id) ON DELETE CASCADE

)
